<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/siswasmkn10jkt','SiswaController@index');
Route::post('/siswasmkn10/create','SiswaController@create');
Route::get('/siswasmkn10jkt/{id}/edit','SiswaController@edit');
Route::post('/siswasmkn10/{id}/update','SiswaController@update');
Route::get('/siswasmkn10jkt/{id}/delete','SiswaController@delete');