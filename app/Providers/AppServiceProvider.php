<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; //include the necessary use statement at the top of the file.

class AppServiceProvider extends ServiceProvider
{
/**
* Bootstrap any application services.
*
* @return void
*/
public function boot()
{
Schema::defaultStringLength(191); //syntax yang perlu ditambahkan
}

/**
* Register any application services.
*
* @return void
*/
public function register()
{
//
}
}