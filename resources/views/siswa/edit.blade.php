@extends('layouts.master')

@section('konten')
			<h1>Edit Data Siswa</h1>
			@if(session('sukses'))
				<div class="alert alert-success" role="alert">
				  {{session('sukses')}}
				</div>
			@endif
			<div class="row">
				<div class="col-lg-12">
				<form action="/siswasmkn10/{{$siswa->id}}/update" method="POST">
					{{csrf_field()}}
				  <div class="form-group">
				    <label for="exampleInputEmail1">Nama Siswa</label>
				    <input name="nama_siswa" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Nama Siswa" value="{{$siswa->nama_siswa}}">
				  </div>

				  <div class="form-group">
				    <label for="exampleInputEmail1">Kelas</label>
				    <input name="kelas" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Kelas" value="{{$siswa->kelas}}">
				  </div>

				  <div class="form-group">
				    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
				    <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
				      <option value="L" @if($siswa->jenis_kelamin == "L") selected @endif>Laki-Laki</option>
				      <option value="P" @if($siswa->jenis_kelamin == "P") selected @endif>Perempuan</option>
				    </select>
				  </div>

				  <div class="form-group">
				    <label for="exampleInputEmail1">Agama</label>
				    <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Agama" value="{{$siswa->agama}}">
				  </div>

				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Alamat</label>
				    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$siswa->alamat}}</textarea>
				  </div>
				  <button type="submit" class="btn btn-danger">Edit Data</button>
				</form>
				</div>
			</div>
		</div>
@endsection

					