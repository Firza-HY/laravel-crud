@extends('layouts.master')

@section('konten')
			@if(session('sukses'))
				<div class="alert alert-success" role="alert">
				  {{session('sukses')}}
				</div>
			@endif
			<div class="row">
				<div class="col-6">
					<h1>Data Siswa SMKN 10 JKT</h1>
				</div>

				<div class="col-6">
										<!-- Button trigger modal -->
					<button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#staticBackdrop">
					  Tambah Data Siswa SMKN 10 JKT
					</button>
	
				</div>
				
				<table class="table table-striped table-dark">
					<tr>
						<th>Nama Siswa</th>
						<th>Kelas</th>
						<th>Jenis Kelamin</th>
						<th>Agama</th>
						<th>Alamat</th>
						<th>Muat Ulang</th>
					</tr>
					@foreach($data_siswa as $siswa)
					<tr>
						<td>{{$siswa->nama_siswa}}</td>
						<td>{{$siswa->kelas}}</td>
						<td>{{$siswa->jenis_kelamin}}</td>
						<td>{{$siswa->agama}}</td>
						<td>{{$siswa->alamat}}</td>
						<td><a href="/siswasmkn10jkt/{{$siswa->id}}/edit" class="btn btn-light btn-sm">Update</a>
							<a href="/siswasmkn10jkt/{{$siswa->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a>
						</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>

<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<form action="/siswasmkn10/create" method="POST">
				{{csrf_field()}}
			  <div class="form-group">
			    <label for="exampleInputEmail1">Nama Siswa</label>
			    <input name="nama_siswa" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Nama Siswa">
			  </div>

			  <div class="form-group">
			    <label for="exampleInputEmail1">Kelas</label>
			    <input name="kelas" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Kelas">
			  </div>

			  <div class="form-group">
			    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
			    <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
			      <option value="L">Laki-Laki</option>
			      <option value="P">Perempuan</option>
			    </select>
			  </div>

			  <div class="form-group">
			    <label for="exampleInputEmail1">Agama</label>
			    <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Agama">
			  </div>

			  <div class="form-group">
			    <label for="exampleFormControlTextarea1">Alamat</label>
			    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
			  </div>

	
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Input Data</button>
			</form>
		  </div>
		</div>
	</div>
@endsection
				